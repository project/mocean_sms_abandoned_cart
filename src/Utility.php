<?php

namespace Drupal\mocean_sms_abandoned_cart;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\Query;

class Utility {

 /**
  * SMS API.
  */
  public function smsAbandonedCartSendMessage($recipient, $message) {

    $sms_abandoned_cart_settings = \Drupal::config('mocean_sms_abandoned_cart.settings');

    $url = 'https://rest.moceanapi.com/rest/1/sms';
    $fields = array(
      'mocean-api-key'=>$sms_abandoned_cart_settings->get('api_key'),
	    'mocean-api-secret'=>$sms_abandoned_cart_settings->get('api_secret'),
      'mocean-from'=>$sms_abandoned_cart_settings->get('message_from'),
	    'mocean-to'=>$recipient,
      'mocean-text'=>$message,
      'mocean-resp-format'=>'json',
      'mocean-medium'=>'drupal_abandon_cart'
    );

    $fields_string = '';
    foreach($fields as $key=>$value) {
      $fields_string .= $key.'='.$value.'&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);

    curl_close($ch);

    $json = json_decode($result, true);

    //returns an array
    return $json;
  }

 /**
  * Query API check credits.
  */
  public function smsAbandonedCartGetCredit() {

    $sms_abandoned_cart_settings = \Drupal::config('mocean_sms_abandoned_cart.settings');

    $url = 'https://rest.moceanapi.com/rest/1/account/balance?';
    $fields = array(
      'mocean-api-key'=>$sms_abandoned_cart_settings->get('api_key'),
      'mocean-api-secret'=>$sms_abandoned_cart_settings->get('api_secret'),
      'mocean-resp-format'=>'json'
    );

    $fields_string = '';
    foreach($fields as $key=>$value) {
      $fields_string .= $key.'='.$value.'&';
    }
    rtrim($fields_string, '&');

    $url_final = $url.$fields_string;

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url_final);
    curl_setopt($ch,CURLOPT_HTTPGET, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($result, true);

    //returns an array
	return $json;
  }

 /**
  * Query API check pricing.
  */
  public function smsAbandonedCartGetPricing() {

	$sms_login_settings = \Drupal::config('mocean_sms_abandoned_cart.settings');

	$url = 'https://rest.moceanapi.com/rest/2/account/pricing?';
	$fields = array(
	  'mocean-api-key'=>$sms_login_settings->get('api_key'),
	  'mocean-api-secret'=>$sms_login_settings->get('api_secret'),
	  'mocean-resp-format'=>'json',
	  'mocean-type'=>'verify'
	);

	$fields_string = '';
	foreach($fields as $key=>$value) {
	  $fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$url_final = $url.$fields_string;

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url_final);
	curl_setopt($ch,CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);

	curl_close($ch);

	$json = json_decode($result, true);

	//returns an array
	return $json;
  }

 /**
  * Fetch field name for telephone.
  */
  public function getFieldName() {
	$sms_order_notification_settings = \Drupal::config('mocean_sms_abandoned_cart.settings');

	return $sms_order_notification_settings->get('field_name');
  }

 /**
  * Fetch details of all orders in draft state.
  */
  public function getOrderList() {
    $query = \Drupal::entityQuery('commerce_order')
      ->condition('state', 'draft')
      ->accessCheck(FALSE);
    $order_ids = $query->execute();

    $order_list = array();

    foreach($order_ids as $order_id) {
      $order =\Drupal\commerce_order\Entity\Order::load($order_id);
      $order_list[] = array(
        'order_id' => $order->get('order_id')->value,
        'store_name' => $order->getStore()->getName(),
        'store_email' => $order->getStore()->getEmail(),
        'recipient' => $order->getCustomer()->get((new Utility)->getFieldName())->value,
        'created' => $order->getCreatedTime());
    }

	return $order_list;
  }
}
