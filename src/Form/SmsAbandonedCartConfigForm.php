<?php

namespace Drupal\mocean_sms_abandoned_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mocean_sms_abandoned_cart\Utility;

use Drupal\Core\Messenger\MessengerInterface;

class SmsAbandonedCartConfigForm extends ConfigFormBase {

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a SmsOrderNotificationContentForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder, RequestContext $request_context) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mocean_sms_abandoned_cart_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	  $form = parent::buildForm($form, $form_state);
    $sms_abandoned_cart_config = $this->config('mocean_sms_abandoned_cart.config');

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Toggle to enable/disable module.'),
	    '#default_value' => $sms_abandoned_cart_config->get('enable'),
    ];

    $form['send_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send timeout'),
	    '#description' => $this->t('How many minutes since cart abandoned to send alert SMS. Default is 1 day.'),
	    '#default_value' => $sms_abandoned_cart_config->get('send_timeout'),
    ];

    $form['history_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('History limit'),
      '#description' => $this->t('How many minutes to search back in history for abandoned carts. Default is 7 days.'),
      '#default_value' => $sms_abandoned_cart_config->get('history_limit'),
    ];

    $form['batch_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch limit'),
      '#description' => $this->t('How many alert SMS to be sent on each run.'),
      '#default_value' => $sms_abandoned_cart_config->get('batch_limit'),
    ];

    $form['content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content'),
      '#description' => $this->t('Content to be sent in alert SMS.'),
      '#default_value' => $sms_abandoned_cart_config->get('content'),
      '#description' => $this->t('Available placeholders: {store_name}, {store_email}'),
    ];

    $form['#attached']['library'][] = 'mocean_sms_abandoned_cart/mocean_sms_abandoned_cart';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$this->config('mocean_sms_abandoned_cart.config')
      ->set('enable', $form_state->getValue('enable'))
      ->set('send_timeout', $form_state->getValue('send_timeout'))
      ->set('history_limit', $form_state->getValue('history_limit'))
      ->set('batch_limit', $form_state->getValue('batch_limit'))
      ->set('promotion', $form_state->getValue('promotion'))
      ->set('content', $form_state->getValue('content'))
      ->save();

    \Drupal::messenger()->addStatus($this->t('Configuration saved.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mocean_sms_abandoned_cart.config'];
  }

}
