<?php

namespace Drupal\mocean_sms_abandoned_cart\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for mocean_sms_broadcast pages.
 */
class SmsAbandonedCartController extends ControllerBase {

  public function smsAbandonedCartConfig() {
    $build = [];
    $build['mocean_sms_abandoned_cart_config_form'] = $this->formBuilder()->getForm('Drupal\mocean_sms_abandoned_cart\Form\SmsAbandonedCartConfigForm');
    return $build;
  }
}
